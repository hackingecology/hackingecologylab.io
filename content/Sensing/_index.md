---
title: "Sensing Platform"
menuTitle: "Sensing Platform"
weight: 2
draft: False
chapter: true
type: docs
menu: main
pre: "2. "
description: >
    A special section with a docs layout.
---

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/hackingecology_text.png?width=35pc)

### Sensing Platform

The Hacking Ecology Sensing Platform is the online platform that enables an easy and secure management of environmental data from Nayad.
The Hacking Ecology Sensing Platform ensure a secure framework against data loss, and dynamic to be applied to different ends: from a single sampling to time series analysis.

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/sensormgmt.png?width=30pc)