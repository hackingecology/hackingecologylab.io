---
title: "Setting Up"
menuTitle: "Setting Up Sensing Platform"
weight: 3
draft: False
type: docs
menu: main
description: >
    A special section with a docs layout.
---

# Sensing Platform

### Introduction

This tutorial covers the configuration of the **Sensing Platform** to connect Nayad for the device management, data collection and visualization.


### Device Configuration
 
#### Create a Device

The straightforward way to do it is using User Interface:

- Log into Hacking Ecology Sensing Platform using your credentials and open the left side tab or the **"Device"** icon.

![](/images/uploads/5d820dddf336a7e421bb3c6ab855d290/hello-world-step-1-item-1.png?width=50pc)

- Click on the "+" icon in the upper right corner of the table and select "Add New Device".

![](/images/uploads/7e9888ac4d7a2d458f62b764fd2438d3/hello-world-step-1-item-2.png?width=50pc)

- Give a name to the device and click "Add" to add it.
![](/images/uploads/36bf26f3d253634e2814a4a12b3c4f0c/hello-world-step-1-item-3.png?width=50pc)

- Device will be listed first, as the table sorts devices by creation date.

#### Connecting Nayad

To connect Nayad, you have to use its credentials. The Sensing platform supports multiple device credentials. We recommend using automatically generated credentials, which is the access token for this guide. But the Token can be changed, when the identification by specific name is really necessary.

- Select the device: Click on its row in the table to open the details.
![](/images/uploads/f87b1bd45a2791c2d49304565181c548/hello-world-step-1-item-4.png?width=50pc)


- Click on "Copy Access Token" and store it in a safe place. You're going to use it when setting up Nayad firmware.
![](/images/uploads/cd4ec1513b8c1010564388395f660c46/hello-world-step-2-item-2.png?width=50pc)

- Go to Nayad Firmware and edit the `TOKEN_ID` with the Access Token from your device :

```cpp
#define TOKEN "TOKEN_ID"
```

After successfully publishing the data readings, you will be able to see them in the **Telemetry** tab of your device:

- Click on the row in the table to open the device details.

- Navigate to the **Telemetry** tab.
![](/images/uploads/ef3a23ea95efe4a2d02daed0319a7991/hello-world-step-3-item-3.png?width=50pc)


#3. Dashboard Configuration

#### Create Dashboard

Once you confirm the telemetry data, let's create the dashboard to visualize the data.

- Open the **Dashboard** page. Click on the "+" icon in the upper right corner. Select "Create New Dashboard".
![](/images/uploads/b41d5feab86f63a712d8f75bd0270ec8/hello-world-step-31-item-1.png?width=50pc)

- Complete the input Dashboard name. Click "Add" to add the panel.
![](/images/uploads/53fca8a3e79619f9a6d0acde7c3b2086/hello-world-step-31-item-2.png?width=50pc)

- Now you should see your dashboard.

- Click on the "Open Dashboard" icon.

Remember that the table sorts dashboards by time of the creation, so the new dashboard will be listed first.

#### Setting Nayad to be used in te widgets (Creating Alias ​​and Widgets)

##### Add Alias

**Alias** is a reference to a single entity (**Entity**) or group of entities that are used in widgets. The alias can be static or dynamic. For simplicity, we will use “single entity” alias references to the single entity (eg Nayad01).

- Enter edit mode: Click the pencil icon in the lower right corner.
![](/images/uploads/f540d447ce7ece87408b3f4e756a960d/hello-world-step-32-item-1.png?width=50pc)

- Click on the "Entity Alias" icon at the top right of the screen. You will see an empty list of aliases.
![](/images/uploads/b38a59c4caef49a5ee2e6df5852d1167/hello-world-step-32-item-2.png?width=50pc)

- Click on "Add Alias".
![](/images/uploads/2d878a7d1ecb1b4fdc5cf68e89aa0405/hello-world-step-32-item-3.png?width=50pc)

- Select the input alias name, e.g. "MyDevice".
- Select the filter type "Single entity". Select "Device" as the type and enter "My new" to enable autocompletion. Choose your device on autocomplete and click on it.
![](/images/uploads/4a02306f79539a3466c77473b188998e/hello-world-step-32-item-4.png?width=50pc)

- Click "Add" and then "Save".
![](/images/uploads/a3f18f8e6bd6786cb73d7395e70925d5/hello-world-step-32-item-5.png?width=50pc)


- Finally, click "Apply changes" in the dashboard editor to save the changes. Then you must enter edit mode again.
![](/images/uploads/336b0b1fa92eee1b17dc214d29b7c718/hello-world-step-32-item-6.png?width=50pc)

### Add Widget

The **widgets** are grouped into widget packages. Each widget has its data source. Thus, each widget group has its way of displaying data. To see the latest value of the data that we send when we connect the device(s), we must configure the data source (here we use temperature as an example).

- Enter edit mode. Click on the "Add New widget" button.
![](/images/uploads/b6d8b1786d5c36430d9af13d178e30a5/hello-world-step-34-item-1.png?width=50pc)
![](/images/uploads/1555b666cff858240bec06997f6124b0/hello-world-step-34-item-2.png?width=50pc)

- Select the "Card" widget package. Select the "Last Values" tab. Click the Entities widget header. The "Add widget" window will appear.
![](/images/uploads/f53085e3a464191d2cef68c7ccb58a03/hello-world-step-34-item-4.png?width=50pc)

- Click "Add" to add the data source. A widget can have multiple data sources, you just need add the other sources using the previous steps.

- Select the entity's alias (in this example it is called "MyDevice"). Then click on the input field on the right. Auto-completion with available data points will be displayed. Select the data point and click "Add".
![](/images/uploads/bc7856044dacf7a525122ca926e3acef/hello-world-step-34-item-6.png?width=50pc)

- Resize the widget to make it a little bigger. Just drag the bottom right corner of the widget. You can also play around with advanced settings if you want to edit the widget.
![](/images/uploads/fcc2eccf6a450269585ed396bbaa9a58/hello-world-step-34-item-7.png?width=50pc)
![](/images/uploads/7ffa08f1354187e5c6bd31c7d297abc1/hello-world-step-34-item-8.png?width=50pc)