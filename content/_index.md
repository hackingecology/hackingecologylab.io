---
title: "Documentation"
menuTitle: "Documentation"
weight: 1
draft: False
type: docs
chapter: true
menu: main
pre: 2.
description: >
    A special section with a docs layout.
---

### Documentation
![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/NAYAD_logo_circle_dark.png?width=10pc)
# Nayad

Welcome to the Nayad Documentation. 

Nayad is the Modular Nayad Aquatic Sonde focused on ecological research and education. Nayad facilitates a wide range of deployment: experimental research, surveys, log and short term studies.




