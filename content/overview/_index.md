---
title: "NAYAD Modular"
menuTitle: "Overview"
weight: 1
type: docs
draft: False
menu: main
pre: "1. "
description: >
    A special section with a docs layout.
---

### No Place for Obsolete Systems

The current aquatic probes are expensive and technically limited. This reality results in a clear unequal distribution of data and its concentration in wealthy regions in the world.


In the last years, many interesting projects have been developed to focus on the use of open source hardware for environmental monitoring. Nayad follows this track, and brings the power of free-open source technologies focused on the reliable and robust data for water quality, and.

Nayad is a cross-platform, modular, scalable system, integrated to a data management platform ready to hold a broad monitoring activity.

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/data_flow_schema.png?width=60pc)

{{% notice note %}}
The core system provides the logging, interface and data management features using a solid and highly configurable framework for aquatic monitoring.
{{% /notice %}}

[**Nayad Board**](/overview/hardware/): A microcontroller with wireless connection and datalogger at the heart of Nayad sonde.

**Sensors Shield** (coming soon): A shield with OLED display, input for Sensor Modules, and compatible with LoRa and GPS.

[**Sensor Modules**](/overview/firmware/) (coming soon with the sensor shield): The modules are the circuit part of environmental sensors that "translate" signals into human readable data. Hacking Ecology have been developing the modules for Dissolved Oxygen (DO), pH, ORP, Electric Conductivity (EC), Salinity and Total Dissolved Solid (TDS), to be available in January 2022. 

[**Firmware**](/overview/firmware/): Is the software running inside the Nayad Board.




---
**Nayad Modular** is released under free and open source licenses.

**Nayad Modular**: CERN Open Hardware License Strong 2.0 [CERN-OHL-S V.2](https://ohwr.org/cernohl)

**Nayad Firmware**: GNU Public License 3.0: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

**Nayad Documentation**: Creative Commons Attribution-ShareAlike 4.0 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
**Nayad Modular** is an Open Hardware certified by oshwa.org [UID ES000018](https://certification.oshwa.org/es000018.html)
[![](https://gitlab.com/hacking-ecology/nayad-modular/-/raw/master/IMG/oshwa-nayad-modular.png?width=05pc)](https://certification.oshwa.org/es000018.html)

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/g2787.png?width=10pc)