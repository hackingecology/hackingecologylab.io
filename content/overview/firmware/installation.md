---
title: "Installation"
menuTitle: "Installation"
weight: 1
type: docs
menu: main
description: >
    First steps before running Nayad.
---



{{% notice note %}}
You have to follow a few steps before programming Nayad.
{{% /notice %}}
This setup is necessary only if you haven't yet configured your Arduino IDE to work with Nayad.


{{% notice tip %}}
Make sure you already have installed Python and Pyserial before starting with the installation (check [troubleshooting](https://hackingecology.gitlab.io/overview/firmware/troubleshooting/) to see how to install them).
{{% /notice %}}

## Installing the Arduino IDE

### Linux, Mac, Windows

1. Install the software [Arduino IDE](https://www.arduino.cc/en/software)

2. Go to File > Preferences

![](/images/uploads/55f06f47feee7228c487740d80e8fe18/installing-ESP32-in-Arduino-IDE-02.jpg?width=30pc)

 There is a field called *Additional Boards Manager URLs Adiconais* in which we add the following line:

https://dl.espressif.com/dl/package_esp32_index.json

![](/images/uploads/6f53b0bf4e2807afb38aca77a6b8f5bb/installing-ESP32-in-Arduino-IDE-03.jpg?width=30pc)

3. In Tools > Boards Manager add ESP32:

![](/images/uploads/174d3419b2d9f75a0790e3430584d811/installing-ESP32-in-Arduino-IDE-07.jpg?width=30pc)


4. Then we should be able to see the list of new boards in Tools > Board

![](/images/0fe552134b665e8a9bff3f95daa86d61/installing-ESP32-in-Arduino-IDE-06.jpg?width=30pc)

5. Now just select the **ESP32 Dev Module**, the model used by Nayad Modular.

{{% notice warning %}}
Eventually Mac and Windows users might face problems connecting with NAYAD card. This is a known issue, due to missing of a communication drivers. 
{{% /notice %}}

### Windows and Mac - Extra Step

To solve the issue with serial communication, we have to install the following drivers:

#### Windows
[CH341SER.zip](/driver/CH341SER.zip)

#### Mac
[CH341SER_MAC.ZIP](/driver/CH341SER_MAC.ZIP)

### Add libraries:

Nayad uses different libraries, including extra libraries developed specifically for its configuration and sensors. There are two different ways to install standard and extra libraries. They are described bellow:

#### Standard Libraries:

In tools > library manager add the following libraries:

```
ArduinoHttpClient
ArduinoJson
DallasTemperature
OneWire
PubSubClient
ThingsBoard

```
#### Extra Libraries


1. Download the libraries in zip format:

{{% button href="https://gitlab.com/hacking-ecology/nayad-RTC" icon="fas fa-download" %}}Nayad-RTC Repository{{% /button %}}


{{% button href="https://gitlab.com/hacking-ecology/NAYAD_pH" icon="fas fa-download" %}}Nayad-pH Repository{{% /button %}}


{{% button href="https://gitlab.com/hacking-ecology/nayad_do" icon="fas fa-download" %}}Nayad-pH Repository{{% /button %}}




2. Go to Sketch > Include Library > Add '.zip' Library.
3. Open the download .zip files.

![](/images/uploads/d49d8a8c22bd66ca79ff16e9c536747f/FZ9CTQWK0CP3102.png?width=30pc)
