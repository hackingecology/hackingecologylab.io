---
title: "Upload Firmware"
menuTitle: "Upload Firmware"
weight: 2
type: docs
menu: main
description: >
    How to upload the program inside your Nayad.
---

{{% notice note %}}
Once we upload the code inside Nayad, the board will be ready to use.
{{% /notice %}}
We repeat theses steps only if we change some details related to internet connection and device ID in the sensing platform, otherwise Nayad will work as expected because the code is stored into a persistent memory.

{{% notice info %}}
The following steps are the same for GNU/Linux, Mac and Windows
{{% /notice %}}

##### Before we start, let's set some things up.

1) Be sure that the USB cable of Nayad is connected to your computer

2) Set upload speed to 115200: tools > upload speed 115200

#### Get the firmware

##### Download the firmware you will use:

First we have to download the firmware in the link below:

{{% button href="https://gitlab.com/hacking-ecology/nayad_firmware" icon="fas fa-gitlab" %}}Nayad Firmware{{% /button %}}

Once we have downloaded it, we unzip the files and open the firmware we are going to use. The pH sensor firmware is the example, for this documentation.

#### Check the Header!

Every code has a header with some details (code version, license, etc). Important to note that the header has the information about the calibration. Each sensor has its own calibration (we will talk about them in the last section of this page).

![](/images/uploads/0fe552134b665e8a9bff3f95daa86d61/firmware_head.png?width=30pc)

#### Change the internet configuration (WIFI_AP and WIFI_PASSWORD), and the device token (TOKEN)

![](/images/uploads/0fe552134b665e8a9bff3f95daa86d61/firmware_edit.png?width=30pc)


#### Check the configuration at 'Tools'

![](/images/uploads/0fe552134b665e8a9bff3f95daa86d61/tools_Arduino_IIDE.png?width=30pc)

#### Upload the Code

Here the best practice for upload is:

 - First run a verification to check if there is some error
 ![](/images/uploads/0fe552134b665e8a9bff3f95daa86d61/verify_ide.png?width=30pc)
 
 - Then Upload the code
 ![](/images/uploads/0fe552134b665e8a9bff3f95daa86d61/upload_firmware.png?width=30pc)





