---
title: "Firmware"
menuTitle: "Firmware"
pre: "b) "
weight: 2
type: docs
menu: main
description: >
    A special section with a docs layout.
---

Firmware is a specific class of software that provides low-level control for a device's specific hardware.

The firmware of Nayad works as the complete operating system of the device, performing all control, monitoring and data manipulation functions.

Once Nayad is programmed, the firmware will be kept in non-volatile memory devices such as Flash memory or EEPROM. Firmware upgrade requires the EEPROM or flash memory to be reprogrammed.

{{% button href="https://gitlab.com/hacking-ecology/NAYAD_pH" icon="fas fa-download" %}}Get pH firmware{{% /button %}}
{{% button href="https://gitlab.com/hacking-ecology/nayad_do" icon="fas fa-download" %}}Get DO firmware{{% /button %}}

Nayad firmware has two main basic functions, `setup` and `loop`, and extra functions for its customization:

## Setup
This function is usually called at the beginning of the sketch and is used to initialize variables, pin modes, etc.
It runs only once after each boot or when Nayad is restarted.

```cpp
void setup() {
  Serial.begin(115200);

}
```

## Loop
After the `setup` function is done, the loop function runs continuously.

```cpp
void loop() {

}
```

## extra Functions

Using additional functions is a way to make writing code easier. In Nayad we use the following functions:

* **void SensorsDataDO()** - Contains the basic operations for reading, converting and print sensor data.
* **void parse_cmd(char* string)** - Defines the activities of the real-time clock (Real-Time Clock), data acquisition and data processing for water heater action.
* **void writeFile(fs::FS &fs, const char * path, const char * message)**
* **void appendFile(fs::FS &fs, const char * path, const char * message)**
* **void gotTouch1()**
* **void InitWiFi()** - Contains settings for connecting to the WiFi network.
* **void reconnect()** - Allows the system to automatically reconnect in case of network disconnection.

Details of using these functions specifically in this experiment are found in [Program Details](firmware)