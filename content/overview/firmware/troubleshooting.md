---
title: "Troubleshooting"
menuTitle: "Troubleshooting"
weight: 4
type: docs
menu: main
description: >
    How to solve some known issues during the firmware upload.
---

{{% notice note %}}
The following issues and solutions were found using debian-based GNU/Linux operating systems.
{{% /notice %}}

If you have some problem to upload your code, please let we know and we will be happy to give you all support.



#### Error opening serial port

Check if the cable is connecting the Nayad board to your computer.

1) Open Terminal and type: ```ls -l /dev/ttyUSB*```

2) you will get something like: ```crw-rw---- 1 root dialout 188, 0 5 apr 23.01 ttyUSB0```

The "0" at the end of USB might be a different number, or multiple entries might be returned. The data we need is "dialout" (is the group owner of the file).

3) Enter the following commands in the terminal, replacing <username> with your username:

* ```sudo usermod -a -G <group-name> <username>```

Something like: ```sudo usermod -a -G dialout johnny```

Logout and log back, then you should have it working.
    
#### Error: "exec: "python": executable file not found in $PATH
    
Solution: sudo ln -s /usr/bin/python3 /usr/bin/python
    
#### Import Error No module named serial
    
```
/.arduino15/packages/esp32/tools/esptool_py/3.0.0/esptool.py", line 38, in <module>
        import serial
    ImportError: No module named serial
```
Problem: pyserial is not installed in your computer.

Solution: on terminal type ```pip3 install pyserial```
