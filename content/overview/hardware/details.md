---
title: "details"
menuTitle: "Details"
weight: 1
draft: False
type: docs
menu: main
description: >
    A special section with a docs layout.
---

# Nayad Modular

**Nayad Modular** is a datalogger focused on low cost systems for water monitoring. The modularity allows the use of single or multiple sensors, the integration with peripherals (e.g. GPS, LoRa, etc).

**Nayad Modular** uses a ultra-low power (ULP) framework, with a highly efficient processor, and can work without internet connection with its web server mode.


{{% notice info %}}
The board also uses some integrated modules (like the µSD module), easy to replace in case of failure.
{{% /notice %}}



![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/Nayad_Modular.png?width=30pc)



{{% notice note %}}
**Nayad Modular** is developed to be flexible for different deployment. Thus, it can be also integrated to soil and atmospheric sensors, as well as used on automated experiments controlling environmental variations (heaters, nutrient input, etc).
{{% /notice %}}




{{%expand "Technical Specifications" %}}
| Feature   |      Detail      |
|----------|:-------------:|
| CPU |  low-power Xtensa® 32-bit LX6 microprocessors |
| Memory Size |    16MB Flash, 8MB SRAM  |
| Voltage - Supply| 2.3V ~ 3.6V  |
|Operating Temperature| -40°C ~ 85°C|
|RF Family/Standard| Bluetooth and WiFi|
{{% /expand%}}

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/scheme_nayad.png?width=30pc)