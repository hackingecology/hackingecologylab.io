---
title: "Setup"
menuTitle: "Setup"
weight: 1
draft: true
type: docs
menu: main
description: >
    A special section with a docs layout.
---


{{% button href="https://hackingecology.com/" %}}Get H(e){{% /button %}}
{{% button href="https://hackingecology.com" icon="fas fa-download" %}}Get H(e) with icon{{% /button %}}
{{% button href="https://hackingecology.com" icon="fas fa-download" icon-position="right" %}}Get H(e) with icon right{{% /button %}}
