---
title: "Integration"
menuTitle: "Integration"
weight: 3
draft: False
type: docs
menu: main
pre: "3. "
description: >
    A special section with a docs layout.
---


You can extend the use of Nayad connecting the to API using R, Python or other language you prefer.
The direct connection allows you to work directly into you environment and reduce the risk of data tempering.

So far we have developed a script to connect R to the database, and soon we expect having more integrations for users working with different languages.

The script for R is here:
{{% button href="https://gitlab.com/hacking-ecology/data-analysis" icon="fas fa-download" %}}NayadR{{% /button %}}

![](/images/uploads/feec12598767ec478e24ee0e6c2e95f6/nayad_R.png?width=15pc)


{{% notice note %}}
Please let us know if you would like to contribute with a reproducible script, or just submit a pull request. We would love to add your Nayad Script to the data analysis repository!
{{% /notice %}}